{% extends "base.volt" %}
{% block content %}
<div class="page_heading">
        <a href="{{ url('users') }}" class="back_btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a><h1>USER INFO</h1>
        <div class="top_buttons one_type_buttons">
            <button type="button" class="btn btn-warning resetBtn" data-toggle="modal" data-id="{{ user.id }}" data-target="#resetPassword" >RESET PASSWORD</button>
        </div>
    </div>
    <div class="page_container personal_info_page">
        <div class="personal_info_block">
            <div class="row">
                <div class="left_side">
                    <div class="img_box">
                        <img src="{{ user.imageUrl }}" alt="" title="">
                        <p>User photo</p>
                    </div>
                </div>
                <div class="right_side">
                    <ul>
                        <li class="user_info user_name">
                            <span>{{ user.name }} {{ user.surname }}</span>
                        </li>
                        {% if user.birthday is not empty %}
                            <li class="user_info">
                                <span>Birthday:<span>
                                <span>{{ user.birthday }}</span>
                            </li>
                        {% endif %}
                        {% if user.phone is not empty %}
                            <li class="user_info">
                                <span>Phone:<span>
                                <span>{{ user.phone }}</span>
                            </li>
                        {% endif %}
                        {% if user.gender is not empty %}
                            <li class="user_info">
                                <span>Gender:</span>
                                <span>{{ user.gender }}</span>
                            </li>
                        {% endif %}
                        {% if user.city is not empty %}
                            <li class="user_info">
                                <span>City:<span>
                                <span>{{ user.city }}</span>
                            </li>
                        {% endif %}
                    </ul>
                </div>
            </div>
        </div>

        {% if userSegments|length %}
            <h3 class="sub_heading">USER IN SEGMENT(S)</h3>
            <div class="segment_box">
            <div class="row">

                {% for segment in userSegments %}
                    <div class="col-md-2">
                        <div class="segment_block">
                            <div class="inner_info">
                                <p>{{ segment['title'] }}</p>
                                <p>{{ segment['users'] }} users</p>
                            </div>
                        </div>
                    </div>
                {% endfor %}
                
            </div>
        {% endif %}

        <h3 class="sub_heading">USER ACTIVITY</h3>
        <div class="page_container bordered users_container">
            <div class="table default_table users_inner_table users_active_table">
                <div class="table_row table_heading">

                    {% set cols = [
                        '1' : 'ACTIVITY',
                        '2' : 'OFFER',
                        '3' : 'DATE / TIME',
                        '4' : 'OFFER TYPE',
                        '5' : 'OFFER PRICE'
                        ] %}

                    {% for key, value in cols %}
                        <div class="table_cell">
                            <a href="{{ url('users/show/' ~ user.id, { 'order': key, 'type': type, 'page': page}) }}" class="relative">
                                <span>{{ value }}</span>
                                <div class="sort_arrows">
                                    {% if (type == 0 and order == key) or order != key %}
                                        <i class="fa fa-caret-up" aria-hidden="true"></i>
                                    {% endif %}
                                    {% if (type == 1 and order == key) or order != key %}
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    {% endif %}
                                </div>
                            </a>
                        </div>
                    {% endfor %}

                </div>

                {% if activations.total_items > 0 %}
                    {% for activation in activations.items %}
                        {% if activation.subDealId is not empty %}
                            <div class="table_row">
                                <div class="table_cell">Activated</div>
                                <div class="table_cell">{{ activation.name }} ({{ activation.subDealName }})</div>
                                <div class="table_cell">{{ date("d.m.Y H:i", strtotime(activation.dateActivated)) }}</div>                            
                                <div class="table_cell">Coupon</div>
                                <div class="table_cell">{{ activation.subDealPrice > 0 ? activation.subDealPrice ~ ' CHF' : 'FREE' }}</div>
                            </div>
                        {% elseif activation.dealSubtype is defined AND activation.dealSubtype == constant("Deals::SUBTYPE_CLAIM_ON_SPOT") %}
                            <div class="table_row">
                                <div class="table_cell">Activated</div>
                                <div class="table_cell">{{ activation.name }}</div>
                                <div class="table_cell">{{ date("d.m.Y H:i", strtotime(activation.dateActivated)) }}</div>                            
                                <div class="table_cell">{{ dealTypes[activation.dealType] }}</div>
                                <div class="table_cell">{{ activation.salesPrice > 0 ? activation.salesPrice ~ ' CHF' : 'FREE' }}</div>
                            </div>
                        {% else %}
                            <div class="table_row">
                                <div class="table_cell">Purchased</div>
                                <div class="table_cell">{{ activation.name }}</div>
                                <div class="table_cell">{{ date("d.m.Y H:i", strtotime(activation.dateActivated)) }}</div>                            
                                <div class="table_cell">{{ dealTypes[activation.dealType] }}</div>
                                <div class="table_cell">{{ activation.salesPrice > 0 ? activation.salesPrice ~ ' CHF' : 'FREE' }}</div>
                            </div>
                        {% endif %}
                    {% endfor %}
                {% else %}
                    <div class="table_row">
                        <div class="table_cell">
                            Deals not found
                        </div>
                        <div class="table_cell"></div>
                        <div class="table_cell"></div>
                        <div class="table_cell"></div>
                    </div>
                {% endif %}

            </div>
        </div>

        {%  include "_partials/pagination"
            with [
                'paginator' : activations,
                'page' : page,
                'path' : url('users/show/' ~ user.id, {
                    'order' : order,
                    'type' : curType,
                    'page' : ''
                })
            ]
        %}

    </div>
</div>

    {% include '_modals/reset_password.volt' %}

{% endblock %}

{% block js %}
    {# JS #}
    {% include 'users/_users_scripts.volt' %}
    <script>$(document).ready(function(){ usersShowScripts(); });</script>
{% endblock %}
