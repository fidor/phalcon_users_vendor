<?php

namespace tetrapak07\Users;

use Supermodule\ControllerBase as SupermoduleBase;
use Phalcon\Mvc\Controller;

abstract class Users extends Controller
{
    public function initialize()
    {
        $this->dataReturn = true;
        if (!SupermoduleBase::checkAndConnectModule('users')) {
            $this->dataReturn = false;
        }
    }
}
