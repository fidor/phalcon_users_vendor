{% if conditionType == 1 %}
    <div class="row">
        <div class="col-md-2">
            <select class="selectpicker changer" name="condition[{{ line }}][type]" >
                <option value="2" >Ski resort</option>
                <option selected="selected" value="1" >Deal</option>
            </select>
        </div>
        <div class="col-md-2">
            <select class="selectpicker" name="condition[{{ line }}][purchased]" >
                <option value="1" {% if condition is not empty and condition['purchased'] == constant('SegmentsDealsRel::PURCHASED') %}selected="selected"{% endif %} >Purchased</option>
                <option value="0" {% if condition is not empty and condition['purchased'] == constant('SegmentsDealsRel::NOT_PURCHASED') %}selected="selected"{% endif %} >Not purchased</option>
            </select>
        </div>
        <div class="col-md-2">

            {% if deals is defined %}
                <select class="selectpicker" data-live-search="true" name="condition[{{ line }}][dealId]" >
                    {% for deal in deals %}
                        {% if deal.Info is not empty %}
                            <option value="{{ deal.id }}" {% if condition is not empty and condition['dealId'] == deal.id %}selected="selected"{% endif %} >{{ deal.Info.name }}</option>
                        {% endif %}
                    {% endfor %}
                </select>
            {% endif %}

        </div>
        <div class="col-md-2"></div>
        <div class="col-md-2">
            <div  class="round_close"><i class="fa fa-times" aria-hidden="true"></i></div>
        </div>
    </div>
{% else %}
    <div class="row">
        <div class="col-md-2">
            <select class="selectpicker changer" name="condition[{{ line }}][type]" >
                <option selected="selected" value="2" >Ski resort</option>
                <option value="1" >Deal</option>
            </select>
        </div>
        <div class="col-md-2">
            <select class="selectpicker" name="condition[{{ line }}][visited]" >
                <option value="1" {% if condition is not empty and condition['visited'] == constant('SegmentsSkiStationsRel::VISITED') %}selected="selected"{% endif %} >Visited</option>
                <option value="0" {% if condition is not empty and condition['visited'] == constant('SegmentsSkiStationsRel::NOT_VISITED') %}selected="selected"{% endif %} >Not visited</option>
            </select>
        </div>
        <div class="col-md-2">

            {% if skiStations is defined %}
                <select class="selectpicker skiStation" data-live-search="true" name="condition[{{ line }}][skiStationId]" >
                    {% for ln, skiStation in skiStations %}
                        {% if (condition is not empty and condition['skiStationId'] == skiStation.id) or ln == 0 %}
                            {% set companies = skiStation.getCompanies() %}
                        {% endif %}

                        <option value="{{ skiStation.id }}" {% if condition is not empty and condition['skiStationId'] == skiStation.id %}selected{% endif %} >{{ skiStation.name }}</option>
                    {% endfor %}
                </select>
            {% endif %}

        </div>

        {% if companies is defined %}
            <div class="col-md-2 partnersBlock">
                {% include "users/parts/partners" with [
                        'companies' : companies,
                        'condition' : condition,
                        'line' : line
                    ] %}
            </div>
        {% endif %}

        <div class="col-md-2">
            <div  class="round_close"><i class="fa fa-times" aria-hidden="true"></i></div>
        </div>
    </div>
{% endif %}
