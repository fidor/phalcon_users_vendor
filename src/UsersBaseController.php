<?php

namespace tetrapak07\Users;

use Phalcon\Mvc\Controller;
use Supermodule\ControllerBase as SupermoduleBase;

abstract class UsersBaseController extends Controller
{
    public function initialize()
    {
        $this->dataReturn = true;
        if (!SupermoduleBase::checkAndConnectModule('users')) {
            $this->dataReturn = false;
        } else {
            $this->view->setViewsDir($this->config->modules->users->viewsDir);
            $this->view->setTemplateBefore($this->config->modules->users->templateBefore);
        }   
    }

    public function indexAction()
    {

    }

    public function showAction($userId)
    {

    }

    public function resetPasswordAction()
    {

    }

    public function deleteAction()
    {

    }

    public function forgotAction()
    {

    }
    public function registerAction()
    {

    }
}
